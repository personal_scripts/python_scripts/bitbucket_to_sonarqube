import argparse
import requests
import json
import sys
import collections
import time


"""
Command line parameter
"""

parser = argparse.ArgumentParser(description='command line options')
parser.add_argument("-i", "--id", required=True, default=None, type=str, help="Project id. Must be min 2 letters")
# parser.add_argument("--techlogo", required=True, default=None, type=str, help="Technical username")
# parser.add_argument("--techpass", required=True, default=None, type=str, help="Technical pass")
parser.add_argument("--sonartechlogo", required=True, default=None, type=str, help="Technical sonarqube username")
parser.add_argument("--sonartechpass", required=True, default=None, type=str, help="Technical sonarqube pass")
parser.add_argument("-u", "--user", required=True, default=None, type=str, help="User username")
parser.add_argument("-p", "--password", required=True, default=None, type=str, help="User pass")
parser.add_argument("-r", "--repository", default=None, type=str)

args = parser.parse_args()
systemid = args.id
# techlogo = args.techlogo
# techpass = args.techpass
sonartechlogo = args.sonartechlogo
sonartechpass = args.sonartechpass
userlogo = args.user
userpass = args.password
giturl = "http://git.net/rest/api/1.0/projects"
squrl = "http://sonarqube/api"


if args.repository == None:
    repository_specified = False
else:
    repository_specified = True

if repository_specified == True:
    reposlug = [str(item) for item in args.repository.split(',')]


"""
Git parsing function
"""

def request_dec(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        if result.status_code == 200:
            return result.json()
        else:
            return result
    return wrapper

# def sonar_dec(func):
#     def wrapper(*args, **kwargs):
#         result = func(*args, **kwargs)
#         if result.status_code == 200:
#             return result.json()
#         else:
#             return result
#     return wrapper

# @request_dec
# def usergitpars(url):
#     response = requests.get(url, auth=((), ()))
#     return response

@request_dec
def gitpars(url):
    response = requests.get(url, auth=((userlogo), (userpass)))
    return response

@request_dec
def sonarpars(url):
    response = requests.get(url, auth=((sonartechlogo), (sonartechpass)))
    return response

@request_dec
def sonar_post(url):
    response = requests.post(url, auth=((sonartechlogo), (sonartechpass)))
    return response


"""
Check notadmin user autentification
"""

# userauthorized = usergitpars("http://gitft.moscow.alfaintra.net/rest/api/1.0/users/{}".format(userlogo))
# if type(userauthorized) == dict:
#     print("The user {} authorized successfully".format(userlogo))
# else:
#     if userauthorized.status_code == 401:
#         print("The user {} not authorized".format(userlogo))
#         sys.exit()
#     else:
#         print(userauthorized)
#         sys.exit()


"""
Search information about bitbucket project area
"""

json_response0 = gitpars("{}/{}".format(giturl, systemid))
if type(json_response0) == dict:
    print("The project {} was successfull found".format(json_response0["name"]))
else:
    print("Cannot search bitbucket project {}, response code: {}".format(systemid, json_response0.status_code))
    sys.exit()


"""
Getting the user of the project area with their privileges and checking for the administrator rights to the project area of bitbucket from the specialist who launched the task
If the specialist does not have administrator rights, then the script is interrupted.
["errors"][0]["message"] maybe later
"""

json_response1 = gitpars("{}/{}/permissions/users?limit=1000".format(giturl, systemid))
if type(json_response1) == dict:
    users = []
    for values in json_response1['values']:
        users.append([values['user']['slug'], values['user']['displayName'], values['permission']])
        tmpuser = (values['user']['slug'])
        tmppremission = (values['permission'])
        if tmpuser == (userlogo) and tmppremission == "PROJECT_ADMIN":
            userisadmin = "True"
else:
    print("Cannot search bitbucket project premissions for project {}, response code: {}".format(json_response0["name"], json_response1.status_code))

if userisadmin == None:
    print('The user {} does not have privileges in the project area'.format(userlogo))
    sys.exit()
else:
    print("Validation of user privileges in the project area was successfull")


"""
Getting a list of repositories in the project area
"""

json_response2 = gitpars("{}/{}/repos?limit=1000".format(giturl, systemid))
if type(json_response2) == dict:
    print("Getting list repositores for project {} complited succesfull".format(json_response0["name"]))
else:
    print("Cannot getting list repositories for project {}, response code: {}".format(systemid, json_response2.status_code))
    sys.exit()

reposlist = []
for values in json_response2['values']:
    reposlist.append(values['slug'])


"""
Checking the specified repositories for existence
"""

if repository_specified == True:
    for repo in reposlug:
        repocheck = None
        for repo2 in reposlist:
            if repo == repo2:
                repocheck = "True"
        if repocheck != None:
            exist = True
            print('repository {} found in {} project area'.format(repo, json_response0["name"]))
        else:
            exist = False
            print('repository {} not found in {} project area'.format(repo, json_response0["name"]))
            sys.exit()


"""
Checking the rights to repositories in the project area
values['user']['displayName']
"""

usercred = []

def gluing(addjsonresponse, repos_slug):
    for values in addjsonresponse['values']:
        usercred.append([repos, values['user']['slug'], values['permission']])

if repository_specified == True:
    for repos in reposlug:
        gluing(gitpars("{}/{}/repos/{}/permissions/users?limit=1000".format(giturl, systemid, repos)), repos)
        print("Getting list users permission for repository {} completed successful".format(repos))
else:
    for repos in reposlist:
        gluing(gitpars("{}/{}/repos/{}/permissions/users?limit=1000".format(giturl, systemid, repos)), repos)
        print("Getting list users permission for repository {} completed successful".format(repos))

d = collections.defaultdict(dict)
for values in usercred:
   d[values[0]][values[1]] = values[2]

json_response3 = json.dumps(d)
jsonusercred = json.loads(json_response3)

"""
Getting the user of the repository with their privileges and checking for the administrator rights
"""

# if jsonusercred[(reposlug)][userlogo] == 'REPO_ADMIN':
#     print("User {} have privileges in the {} repository".format(userlogo, reposlug))
# else:
#     print("User {} does not have privileges in the {} repository".format(userlogo, reposlug))
#     sys.exit()


"""
Getting a list of branches for the repository that will be created in sonarqube (by default, master and develop are included)
"""

# brancheslist = []
#
# json_response4 = gitpars("{}/{}/repos/{}/branches?limit=1000".format(giturl, systemid, reposlug))
#
# if type(json_response4) == dict:
#     for values in json_response4['values']:
#         brancheslist.append(values['displayId'])
# else:
#     print("Cannot getting list branches for repository {}, response code: {}".format(reposlug, json_response4.status_code))
#     sys.exit()

# for x in branches:
#     if x in brancheslist:
#         pass
#     else:
#         print("Branch {} not exist in repository {}".format(x, reposlug))
#         sys.exit()
#
# print("Getting list branches for repository {} completed successfull".format(reposlug))


"""
Checking for the existing of a project in SonarQube (with branches)
Creating a project in sonarqube
"""

# sq_project_key = '-'.join((systemid, reposlug))

# for sqbranch in branches:
#     json_response5 = sonarpars("{}/projects/search?projects={}:{}".format(squrl, sq_project_key, sqbranch))
#     if not json_response5['components']:
#         json_response6 = sonar_post("{}/projects/create?name={}&project={}&branch={}".format(squrl, sq_project_key, sq_project_key, sqbranch))
#         if type(json_response6) == dict:
#             json_response6 = sonar_post("{}/projects/create?name={}&project={}".format(squrl, sq_project_key, sq_project_key))
#             print("Project {}:{} create succesfull".format(sq_project_key, sqbranch))
#         else:
#             print("Cannot create project {}:{}, response: {}".format(sq_project_key, sqbranch, json_response6.status_code))
#             sys.exit()
#     else:
#         print("Project {}:{} already existed".format(sq_project_key, sqbranch))


"""
Checking for the existing of a project in SonarQube (without branches)
Creating a project in sonarqube
"""

def add_project(repodef):
    for repo in repodef:
        sq_project_key = '-'.join((systemid, repo))
        json_response5 = sonarpars("{}/projects/search?projects={}".format(squrl, sq_project_key))
        if not json_response5['components']:
            json_response6 = sonar_post("{}/projects/create?name={}&project={}".format(squrl, sq_project_key, sq_project_key))
            if type(json_response6) == dict:
                json_response6 = sonar_post("{}/projects/create?name={}&project={}".format(squrl, sq_project_key, sq_project_key))
                print("Project {} create succesfull".format(sq_project_key))
            else:
                print("Cannot create project {}, response: {}".format(sq_project_key, json_response6.status_code))
                sys.exit()
        else:
            print("Project {} already existed".format(sq_project_key))

if repository_specified == True:
    add_project(reposlug)
else:
    add_project(reposlist)


"""
Search for a user in sonarqube and adding a user to a project with rights assignment
"""

notadmpermission = ['codeviewer', 'issueadmin', 'scan', 'user']

def add_cred(repodef):
    for repo in repodef:
        sq_project_key = '-'.join((systemid, repo))
        for usrs in [*jsonusercred[(repo)]]:
            json_response7 = sonarpars("{}/users/search?q={}".format(squrl, usrs))
            if not json_response7['users']:
                print("User {} not found in SonarQube, first you have to login".format(usrs))
            else:
                if jsonusercred[(repo)][(usrs)] == "REPO_ADMIN":
                    json_response8 = sonar_post("{}/permissions/add_user?projectKey={}&login={}&permission=admin".format(squrl, sq_project_key, usrs))
                    if type(json_response8) == dict:
                        print("Successfully added user {} permissions in sonarqube project {}. ".format(usrs, sq_project_key))
                    if json_response8.status_code == 204:
                        time.sleep(1)
                        json_response10 = sonarpars("{}/users/search?q={}".format(squrl, usrs))
                        cred = json_response10['users'][0]['groups']
                        for splitcred in cred:
                            if splitcred == 'sonar-administrators':
                                t = "True"
                        if t != None:
                            print("Successfully added user {} permissions in sonarqube project {}. ".format(usrs, sq_project_key))
                        else:
                            print("Cannot add permissions for the user {} in sonarqube project {}".format(usrs, sq_project_key))
                            sys.exit()
                    else:
                        print("Cannot add permissions for the user {} in sonarqube project {}. Status code: {}".format(usrs, sq_project_key, json_response8.status_code))
                        sys.exit()
                else:
                    geterror = None
                    for perm in notadmpermission:
                        json_response9 = sonar_post("{}/permissions/add_user?projectKey={}&login={}&permission={}".format(squrl,sq_project_key,usrs, perm))
                        if type(json_response8) == dict:
                            pass
                        if json_response8.status_code == 204:
                            time.sleep(1)
                            json_response10 = sonarpars("{}/users/search?q={}".format(squrl, usrs))
                            cred = json_response10['users'][0]['groups']
                            for splitcred in cred:
                                if splitcred == perm:
                                    t = "True"
                            if t != None:
                                pass
                            else:
                                print("Cannot add permissions for the user {} in sonarqube project {}".format(usrs, sq_project_key))
                                sys.exit()
                        else:
                            geterror = True
                    if geterror == None:
                        print("Successfully added user {} permissions in sonarqube project {}. ".format(usrs, sq_project_key))
                    else:
                        print("Cannot add part of permissions for the user {} in sonarqube project {}".format(usrs, sq_project_key))
                        sys.exit()

if repository_specified == True:
    add_cred(reposlug)
else:
    add_cred(reposlist)
